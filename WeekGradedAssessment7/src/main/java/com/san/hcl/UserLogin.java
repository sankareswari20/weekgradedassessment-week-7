package com.san.hcl;

public class UserLogin {
	 private long mobileNumber;
	 private String email;
	 private String userName;
	 private String password;
	 
	public UserLogin() {
		super();
	}

	public UserLogin(long mobileNumber, String User_email, String User_Name, String User_password) {
		super();
		this.mobileNumber = mobileNumber;
		this.email = User_email;
		this.userName = User_Name;
		this.password = User_password;
	}
	
	public long getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(long mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String uName) {
		this.userName = uName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String pass) {
		this.password = pass;
	}	  


}
