package com.san.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.san.hcl.Bookees;
import com.san.service.BookService;

@WebServlet("/BooksC")
public class BookessController extends HttpServlet {
	private static final long serialVersionUID = 1L;
   
    public BookessController() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		BookService booksService = new BookService();
		List<Bookees> books = booksService.getAllBooks();
		HttpSession hs =request.getSession();
		hs.setAttribute("objectBooks", books);
		RequestDispatcher req= request.getRequestDispatcher("index.jsp");
		req.include(request, response);
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}



}
