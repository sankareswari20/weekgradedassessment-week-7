package com.san.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.san.hcl.Bookees;
import com.san.service.BookService;
import com.san.service.UserLoginService;

@WebServlet("/LoginC")
	public class UserLoginController extends HttpServlet {
		private static final long serialVersionUID = 2L;
	       
	    
	    public UserLoginController() {
	        super();
	    }

		protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	Bookees books=new Bookees();
			
			BookService bs=new BookService();
			List<Bookees> res=bs.getAllBooks();
			HttpSession hs=request.getSession();
			hs.setAttribute("obj",res);
		    RequestDispatcher req = request.getRequestDispatcher("displayBooks.jsp");
			req.forward(request, response);
		}

		
		protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			response.setContentType("text/html");

			UserLoginService service= new UserLoginService();	
			HttpSession Session = request.getSession();
			RequestDispatcher rd= request.getRequestDispatcher("login.jsp");
			if(request.getParameter("mobilenumber")!= null) {
			long mobileNumber= Long.parseLong(request.getParameter("mobileNumber"));
			String pass= request.getParameter("pass");
			String loginResult = service.verifyPassword(mobileNumber, pass);
			rd.include(request, response);
			}
		    doGet(request, response);
		}



}
