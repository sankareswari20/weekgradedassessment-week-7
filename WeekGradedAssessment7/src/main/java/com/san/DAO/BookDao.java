package com.san.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.san.DBconnection.DbConnection;
import com.san.hcl.Bookees;

public class BookDao {

	public static List<Bookees> getAllBooks(){
		List<Bookees> listOfBooks = new ArrayList<Bookees>();
		try {
			Connection con = DbConnection.getDbConnection();
			PreparedStatement pstmt= con.prepareStatement("select * from books");
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				Bookees books = new Bookees();
				books.setBookId(rs.getInt(1));
				books.setTitle(rs.getString(2));
				books.setGenre(rs.getString(3));						
				listOfBooks.add(books);
			}
		}catch(Exception e){
		System.out.println(e);
		}
		return listOfBooks;
	}


}
